defmodule Site.Accounts.User do
    use Ecto.Schema
    import Ecto.Changeset

    schema "users" do
        field :userphrase, :string
        field :password, :string, virtual: true
        field :password_hash, :string

        timestamps()
    end

    def changeset(user, attrs) do
        user
        |> cast(attrs, [:userphrase])
        |> validate_required([:userphrase])
        |> validate_length(:userphrase, min: 1, max: 254)
        |> unique_constraint(:userphrase)
    end
    
    def registration_changeset(user, params) do
        user
        |> changeset(params)
        |> cast(params, [:password])
        |> validate_required([:password])
        |> validate_length(:password, min: 6, max: 254)
        |> put_pass_hash
    end

    defp put_pass_hash(changeset) do
        case changeset do
            %Ecto.Changeset{valid?: true, changes: %{password: pass}} ->
                put_change(changeset, :password_hash, Pbkdf2.hash_pwd_salt(pass))
            _ ->
                changeset # return to caller is changeset is not valid
        end
    end
end