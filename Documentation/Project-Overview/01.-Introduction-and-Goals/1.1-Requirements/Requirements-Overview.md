Requirements Overview
---------------------

**Contents.**

Provide access to files and application on any device without worrying about synchronization, security or compatibility. Wolfpack OS just like wolves work better as a pack but are able to function individually or in small groups also.

**Motivation.**

I should have access to the best resources at any time without every having to worry about how or why or some other piece of software. If I start a download on the computer, I should be able to access it instantly everywhere. If I begin doing something on one device, I should be able to pickup anywhere.
