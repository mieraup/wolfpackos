# Storage System Routing

## Overview

Middle layer, responsible for getting, verifying and presenting a virtualized filesystem that provides a list of all files both on the system an remotely accessible.

Other Storage Systems can link to this Storage System. 
This allows requests for data to be sent by the user and cached by the this storage system.

Storage System connects via Websocket so the connection is real time and open. Whenever the user asks for a file via the /api/v1/UUID the storage system will query each connected Storage System that is connected AS that user, if it has a copy of the requested file. If one of the connected Storage System's do, this Storage System requests that file, forwards it to the user and caches a copy on this Storage Server.

NOTE that the file is NEVER decrypted EVERY off the users device. Most information about the file (excluding metadata such as file size, frequency of access), is not possible to obtain the user's passphrase.

## Pushing and Pulling
Both of these actions are from the user's POV. Storage System is as simple as possible.

## WARNING WARNING WARNING
Storage System has NOT been setup to deal with the case that a particular Storage System CANNOT accept pushed file. 
THIS CAN RESULT IN LOST USER DATA. THIS CAN RESULT IN LOST USER DATA.

This is something that we will address in future revisions.

## Pushing

### Basic Overview
    ┌──────────────┐        ┌──────────────────┐
    │ Client       │        │ Storage System   │
    │              │◀──────▶│                  │
    └──────────────┘        └──────────────────┘
                                      ▲         
                                      │         
                                      │         
                                      │         
                                      ▼         
                            ┌──────────────────┐
                            │ Client's Storage │
                            │    System        │
                            └──────────────────┘
### Decision Flow

                     ┌───────────┐                                
                     │Data Pushed│                                
                     └───────────┘                                
                           │                                      
                           │                                      
                           ▼                                      
                           Λ              ┌──────────────────────┐
                          ╱ ╲             │ Push to Client Storage
                         ╱   ╲            │ System and cache data.
                        ╱ is a╲    ┌────┐ │                      │
                       ╱ clients   │Yes │ │                      │
                      ╱ storage ╲  └────┘ │                      │
                     ▕  system   ▏ ──────▶│                      │
                      ╲ connected         │                      │
                       ╲       ╱          │                      │
                        ╲     ╱           └──────────────────────┘
                         ╲   ╱                                    
                          ╲ ╱                                     
                ┌────┐     V                                      
                │ No │     │                                      
                └────┘     │                                      
                ┌──────────┘                                      
                │                                                 
                │                                                 
                ▼                                                 
    ┌───────────────────────┐                                     
    │                       │                                     
    │ Cache data and push   │                                     
    │ when Client Storage   │                                     
    │ system is connected   │                                     
    │                       │                                     
    │                       │                                     
    └───────────────────────┘                                     
### Description

The Storage System will save the data, then push it to the connected Storage Systems via /api/v1/connect

If the Storage System's are not accessible then the data is cached on this Storage System until one of the users Storage System's connect.

## Pulling

The Storage System will get the metadata (last update) from the connected Storage System. If the connected Storage System report a last modification < what this Storage System has this Storage system serves cached content. 

Otherwise this Storage System gets data from a connected Storage System and forwards it to the user and caches it locally.