# Rational for User-Authentication

To protect access to the users files.

## High Level View
Upon Success, user can do work and use WebOS.

## Reason for Server Provided a random user phrase for new users
This increases security by preventing an attack called "credential stuffing".