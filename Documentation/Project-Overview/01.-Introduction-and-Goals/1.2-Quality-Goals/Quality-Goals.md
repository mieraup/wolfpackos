Quality Goals 
-------------

**Contents.**

User should never have to worry about synchronization or what to select for synchronization.
User should be required to log in or signup the minium number of times. If the user is logging in on their desktop, it should automatically pick up the credentials from the computer.
User should be able to use the applications and system the last 5 versions of Chrome, Firefox and Safari.
User should have it just work with everything else that the os they are using.

**Motivation.**

Users should focus on getting their work done. WolfPack OS focuses on making sure the user can get their work done. This means no updating or unnecessary harassing of the user.