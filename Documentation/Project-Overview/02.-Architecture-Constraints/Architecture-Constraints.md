Architecture Constraints
========================

**Contents.**

WolfPack OS Client must run in an unmodified web browser. This forces it to be a Javascript, HTML and CSS.
Must run in Firefox, Chrome and Safari,

To provide full os integration clients MUST be written in native language for each system. 
- .NET for Windows
- Objective C for MacOS
- Java for Android
- Swift for iPhones
- C or C++ for Linux
- Langue for Server side code

**Motivation.**

Each System is broken into several modules, having several modules per system means that each component can be updated individually without effecting the rest of the system. How each module is setup internally does not matter as long as adhere to certain API requirements.

Each System must follow coding guidelines, or if the first to use that language set appropriate coding guidelines based off of other major projects using that language.

**Form.**

See Coding Guidelines for more information.