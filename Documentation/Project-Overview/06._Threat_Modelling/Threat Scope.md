Threat Model
    Government Sponsored Cyber attacks

Decentralized Model inefficient since it's possible that too much time will be spend seeding and establishing trust.

Treat the software as a bank, a person must trust the bank for the bank to be able to do its job.

Security Levels:

- Hardened Code: 
    Code is written with belief that attackers have full access to source code

- Behavior Monitoring:
    Interaction between code modules is monitored and blocked if deemed suspicious

- Sandboxed
    Bugs will exist w/in code and libraries used. Sandboxing prevents a bug in one module from affecting those in another module

- Rate and Payload Limiting
    Prevent attackers from draining CPU resources or exploiting some buffer overflow bug

- Firewall
    Minimal rules needed to ensure rock solid functionality with minimal gaps

- Behavior Monitering
    Moniters users baseline, locking the users account if actions are deemed to substantially differentiate from that users baseline(ie, the account is being hacked and hacker is accessing account details)


