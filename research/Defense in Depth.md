Resource Security
    - Keeping hardware, facilities and networks protected
Functional Security
    - Make it only do what it is supposed to
Process Security
    - Make sure that all communication happens thru well defined API's
Personal Security
    - Making sure that personal information isn't inadvertently exposed
Multi User Security
    - Making sure that customers are isolated from each other (multi tenancy)


Security Measurements needed
    Continous Monitering of Network 
        API failures, rate and origin of API and content access, frequency of access, system load

Providing SaaS Software as a Service. We are responsible for security, hardware of the service.

Backup Solution
    - Database backup
    - Redundancy
    - Failure rate?
Data Concentration - How concentrated is the data, what recovery methods exist

Need to consider in future redundant cloud providers in the event of 
    - network outage
    - cloud provider bankrupty

Clearly defined code and control access

Built with LEAP in mind

NEED intrusion detection
Data Access Controls
Archival of Data
Data Shredding

Sensitive data MUST be encrypted at all times to prevent unintential leaks, data that is NOT encrypted exists in protected RAM that is only accessible to a specific application

SAML, OAuth and other providers make me leary, because they depend on another service to succeed.


