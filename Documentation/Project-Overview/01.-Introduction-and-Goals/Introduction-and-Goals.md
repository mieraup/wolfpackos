Introduction and Goals
======================

Provide an operating system shell that just works, the user can focus on the application everything else is managed behind the scenes.

-  security, synchronization, application and resources the user should not need to worry about.

There are three phrases:

Phase 1:
    Web Browser OS
Phase 2:
    Server Power behind client computer when client connected to internet
Phase 3:   
    Pack Power, where client is connected to friends computing and resources are shared across transparently.