# Updating State

## Overview

The point of updating is to allow improved software. It needs to be done in a controlled automated fashion since updates will happen quite regularly and trusting admins to manage updates when they are busy with every other thing is not fair.

## Updating High Overview

- Finish Transactions
- Update
- Accept connections

## Finish Transactions 

Storage System will finish servicing all transactions current transactions. Storage System will decline all new transactions,

## Update

The Storage System will update. During this point, the connection will be down.

## Accept connections

The Storage System will accept connection attempts again.


## Important Notes

Since the Storage System can go off at random times, the client must have a list of fallback Storage Systems that it can fall back to. Federated Storage Systems will attempt to provide guarantee that at least 1 Storage System will be Online at any given time.