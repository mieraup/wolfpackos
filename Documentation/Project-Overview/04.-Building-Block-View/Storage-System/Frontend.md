### Storage System Frontend

Interfaces with WebOS and provides a facade to the Routing.

## API Generation 1
Provides Access to the Users Files
Endpoints
/api/v1/signup/userphrase [GET]   Gets userphrase for use
/api/v1/signup/znpbundle [POST] Send user phrase, salt and verifier as bundle for signup along with public key
/api/v1/signin/userphrase [POST] Send user phrase, recieve salt and B
/api/v1/signin/authorize [POST] Send up,A,M1 and recieve back Cryptobundle
/api/v1/challenge [POST] Send challenge for specific API access
/api/v1/UUID [GET] Get selected data configuration
/api/v1/UUID [POST] Set selected data configuration
/api/v1/connect [WebSocket] 


API Deep Level

### /api/v1/signup/userphrase

Client -> Server

Server -> Client
{payload:["world1","word2","word3","word4]}
status 200


### /api/v1/signup/znpbundle 

Client -> Server

{
    userphrase:["word1","word2","word3","word4"],
    salt:"salt here",
    verifier:"verifier here",
    publickey:"base64encodedpublickey"
}

Server -> Client
On Success
    status 200
Otherwise
    status 409

### /api/v1/signin/userphrase

Client -> Server

{
    userphrase:["word1","word2","word3","word4"]
}

Server -> Client
On Success
    {
        salt:"saltbase64encoded",
        B:"Bbase64encoded"
    }
    status 200
Otherwise
    status 409

### /api/v1/signin/authorize

Client -> Server

{
    userphrase:["word1","word2","word3","word4"],
    A:"Abase64Encoded",
    M1:"M1Base64Encoded"
}

Server -> Client

On Success
    {
        cryptobundle:"base64EncodedCryptoBundleANDEncryptedWithSessionKey"
    }
    status 200
Otherwise
    status 409

### /api/v1/challenge

Client -> Server

{
    message: "base64EncodedMessage",
    signature:"base64SignitureOfMessage",
}

message is stringified object of the following form:
{
    apirequest:"/api/v1/signin/authorize or some other endpoint",
    timestamp:"currenttime in JS format",
    userphrase:["word1","word2","word3","word4"]
}

Server -> Client

On Success
    {
        message:"base64EncodedANDEncryptedMessageWithUserphrase'sPublicKey"
    }
    status 200

    message is stringified object of the following form:
    {
        apirequest:"/api/v1/signin/authorize or some other endpoint requested",
        validtill:"validtime in JS format",
        secret:"10-20 character secret in ASCII"
    }
Otherwise
    status 409

### /api/v1/UUID [GET] 

Client -> Server

{
    message:{
        challengeanswer:"challangesecret",
        userphrase:["word1","word2","word3","word4"]
    }
    signiture:"signiture",
}

Server -> client

On Success
    Multipart File Download
    status 200
Otherwise
    status 409

### /api/v1/UUID [POST]

Client -> Server

{
    message:{
        challengeanswer:"challangesecret",
        userphrase:["word1","word2","word3","word4"]
    }
    signiture:"signiture",
} 

PLUS MULTIPART UPLOAD OF FILE

Server -> Client
On Success
    status 200
Otherwise
    status 409

### /api/v1/connect [WebSocket]

Forwarded to routing. Allows routing to manage which endpoints to query based on user requesting.