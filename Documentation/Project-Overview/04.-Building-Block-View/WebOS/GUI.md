### Web OS GUI

Launchpad to start,resume and create activities.

-   Provide the expected functionality as outlined in Requirement.md

-   GUI has access to several API commands.
    - list(path) Return null if nothing exists, otherwise array of strings that are all absolute paths
    - getProviderForPath(path) Return image that denotes the provider or null if there is no provider for that path.
    - setProviderForPath(path,providerID) Provider id is org.providername.en.some.name
    - listProviders(path) Return array of full providerID
    - create(path) Returns true if successfully created empty activity
    - create(path,provider) Returns true if successfully created activity with provider
    - launch(path) Create/Resume activity