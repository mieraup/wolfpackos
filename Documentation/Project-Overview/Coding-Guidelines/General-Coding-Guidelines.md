# General Coding Guidelines
## Rational

Maintaining proper coding style is very important for any large software project, such as Qubes. Here’s why:

* It eases maintenance tasks, such as adding new functionality or generalizing code later,
* It allows others (as well as the future you!) to easily understand fragments of code and what they were supposed to do, and thus makes it easier to later extend them with newer functionality or bug fixes,
* It allows others to easily review the code and catch various bugs,
* It provides for an aesthetically pleasing experience when one reads the code…

Often, developers, usually smart ones, undersell the value of proper coding style, thinking that it’s much more important how their code works. These developers
expect that if their code solves some problem using a nice and neat trick, then that’s all that is really required. Such thinking shows, however, immaturity and 
is a signal that the developer, no matter how bright and smart, might not be a good fit for larger projects. Writing a clever exploit for a Black Hat show is one 
thing - writing useful software supposed to be used and maintained for years is quite a different story. If you want to show off what a smart programmer you are, 
then you should become a researcher and write exploits. If, on the other hand, you want to be part of a team that makes real, useful software, you should ensure 
your coding style is impeccable. 

## General typographic conventions

* Use space-expanded tabs that equal 4 spaces. Yes, we know, there are many arguments for using “real” tabs instead of space-expanded tabs, but we need to pick 
one convention to make the project consistent. One argument for using space-expanded tabs is that this way the programmer is in control of how the code will look 
like, despite how other users have configured their editors to visualize the tabs (of course, we assume any sane person uses a fixed-width font for viewing the 
source code). If it makes you feel any better, assume this is just an arbitrary choice made to enforce a unified style.

* Maintain max. line length of 80 characters. Even though today’s monitors often are very wide and it’s often not a problem to have 120 characters displayed in 
an editor, maintaining shorter line lengths improves readability. It also allows others to have two parallel windows open, side by side, each with different 
parts of the source code.

* Maintain a decent amount of horizontal spacing, e.g. add a space after if or before { in C, and similar in other languages. Whether and where to also use 
spaces within expressions, such as (x*2+5) vs. (x * 2 + 5) is left to the developer’s judgment. Do not put spaces immediately after or before the brackets in 
expressions, so avoid constructs like this: if ( condition ) and use ones like this: if (condition) instead.

* Use single new lines (‘\n’ aka LF) in any non-Windows source code. On Windows, exceptionally, use the CRLF line endings (–). This will allow the source code to 
be easily viewable in various Windows-based programs.

* Use descriptive names for variables and functions! Really, at a time when most editors have auto-completion features, there is no excuse for using short 
variable names.

## General programming style guidelines

* Do not try to impress with your coding kung-fu, do not use tricks to save 2 lines of code, always prefer readability over trickiness!
* Make sure your code compiles and builds without warnings.
* Always think first about interfaces (e.g. function arguments, or class methods) and data structures before you start writing the actual code.
* Use comments to explain non-trivial code fragments, or expected behavior of more complex functions, if it is not clear from their name.
* Do not use comments for code fragments where it is immediately clear what the code does. E.g. avoid constructs like this:

// Return window id
int get_window_id (...) {
    (...)
    return id;
}
* Do not use comments to disable code fragments. In production code there should really be no commented or disabled code fragments. 
* Do not hardcode values in the code! The only three numbers that are an exception here and for which it is acceptable to hardcode them are: 0, 1 and -1.

## Source Code management 

Use git to maintain all code for WolfPack project.

Having a monorepo means that it is easy to make all the necessary changes in a single commit.

However that means one must understand the project structure and how different parts relate to each other. 

## Commit message guidelines

Please see Commit Guidelines

### Adopted from QubesOS Project