Whitebox Overall System
-----------------------

The project aims to create a simple system. Simplicity provides better stability, usability and longevity then complicated systems.
### Web OS

![](../../Images/04-OS-Browser-Breakdown.png)

### Storage System

![](../../Images/04-Storage.png)

### Motivation 
- People want access to their files, without complication.

- Anything that is not WebOS is the Storage System. Storage system has three parts which are discussed in detail Storage System.


