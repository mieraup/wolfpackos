Runtime View 
============
```
                            .───────────────.                                   
                           (    Uninitted    )──────────────┐                   
                            `───────────────'               │                   
                                    ▲                       │                   
                          ┌─────────┘                       │                   
                          │                                 │                   
                          │                                 ▼                   
                  .───────────────.                 .───────────────.           
    ┌ ─ ─ ─ ▶ ─ ▶(  Uninstalling   )◀─────────────▶(   Initilizing   )          
                  `───────────────'        ┌ ─ ─ ─ ─`───────────────'           
    │       │                       ─ ─ ─ ─                 │                   
                                   │                        │                   
    │       │                                               │                   
                                   │                        │                   
    │       │                                               │                   
                                   │                        │                   
    │       │                                               │                   
         ─ ─   ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ┘                        └──────┐            
    │   │     │                                                    │            
                                                                   │            
    │   │     │                                                    │            
                                                                   │            
    │   │     │                                                    │            
                                                                   │            
    │   │     │                                                    │            
              ▼                                                    ▼            
    │ .─┴─────────────.               ┌────────────────────▶───────────────.    
     (     Offline     )──────────────┘┌─────────────────▶(     Online      )─ ┐
    │ `───────────────'                │                   `───────────────'    
              ▲                        │                           │       │   │
    │         │                        │                           │            
               ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ┼ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ┼ ─ ─ ─ ┼ ─ ┘
    │                                  │                           │            
                                       │                           │       │    
    │                                  │                           │            
                                       │                           │       │    
    │                                  │                           │            
                                       │                           │       │    
    │                                  │                           │            
                                       │                           │       │    
    │                          .───────────────.                   │            
                              (    Updating     )◀─────────────────┘       │    
    │                          `───────────────'                                
     ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ┘    
```
**Legend.**

The solid lines represent the path and state that the system always strives for which is Online. 

The dotted paths represent alternative paths based on things such as network outage and user choice.

Please note that | is as close to solid as we can do in Unicode. Dashed is like - - - - -.

(0)Uninitialized - Device does not have WebOS.
(1)Initializing - Device is connecting and automatically negotiating storage and relay options
(2)Online - Device is online and transmitting for the user
(3)Offline - Device if offline and caching transmissions for the user
(4)Updating - Device is updating 
(5)Uninstalling - Device is uninstalling all WebOS and content.

## Rational for shared state machine for all systems

Even though WebOS and Storage System have different roles, providing them both with the same states means simpler software that is EASIER to understand. 

The true difference between WebOS and Storage System in terms of state diagram is what happens inside the Online state.


## Runtime examples for WebOS

### Browser does NOT support service workers

1. Uninitialized 
    Since never started before.
2. Initialization
    During setup, lack of service workers detected. Aborts and shows error screen.

0->1->5->0

### Browser does support service workers and then user goes offline
1. Uninitialized
    Since never started before.
2. Initialization
    During setup, service worker installed and system installed.
3. Online
    User can do online stuff.
4. Offline
    User went offline.

0->1->2->3

### Browser does support service workers, interruption during update
1. Online
    User online, check for update.
2. Updating
    Updating fails since partially succeeded. Rolls back update.
3. Offline
    User offline, cache transmissions.


2->3->4
## Runtime examples for Storage Server


