Solution Strategy
=================

**Contents.**

The project aims to provide modules so that incorporating the same feature across multiple distributions are easy. This speeds up deployment time, reduces developers recreating the same thing and provides evolving improvements. 

* The technology does not matter so much that the API's between the different components are clearly defined.
 
* Code checking in, styling and good commenting are important for the solution to be obtained.

**Motivation.**

The project aims to move fast and provide the goals outlined in Requirements-Overview.md.
The best way is to provide clear, top down layered approach with data flowing in single way.

