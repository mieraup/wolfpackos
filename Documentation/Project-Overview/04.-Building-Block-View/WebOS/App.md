### Web OS App 

Run a specific activity with the file provided from Kernel.

-   Provide the expected functionality for the activity being done.

-   App has one components.
    - Runtime Component
        
        * Responsible for running an activity. 
        * MUST NEVER PROVIDE ABILITY TO EXPLICITLY SAVE ACTIVITY. 
            
            This should be done automatically.
        * MUST NEVER PROVIDE ABILITY TO EXPLICITLY CREATE ACTIVITY

            This should be handled by detecting that the file is an empty file. If so most likely user wants to create a new activity.

            If the user is giving a file that the Activity Provider does not understand say so and do not modify file.

-  App has single API commands.
    - update(newFileData) Pass in full file that contains updated sections.

    The app receives the file in the initial launch.