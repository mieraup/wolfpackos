### Security Architecture

#### Overview
There exists a central server that connects all WebOS to all their respective data pools.

![](../Images/04-Server-Setup.png)

### To Implement

| Feature                           | Reason                        |
| --------------------------------- | --------------------------------- |
| HSTS                              | Prevents man in the middle attacks. Prevents users from continuing when there exists an invalid certificate.  
| Simple Authentication System      | Provide security and not utilize a massive library like Passport.js or another JS library that does not provide cross language authentication support. Will use Public/Private Key Encryption and Zero Knowledge Proofs        

### Overview
    
    Please see Storage Server for more detail.

### Long Term Implementation Goals

| Feature                           | Reason                        |
| --------------------------------- | --------------------------------- |
| OWASP-Threat-Dragon               | Properly provide defense in depth and understand weaknesses and attacks against the project.             

#### Not Implemented

| Feature                           | Reason                        |
| --------------------------------- | --------------------------------- |
| Subresource Integrity             | Since the WebOS components are being served from a central location, there is little benefit to providing this feature, since if the site is compromised, the hacker can compromise the index.html file thereby rendering subresource integrity useless.  Furthermore intended to be used with CDN's. Not large scale enough to be used with CDN's.
