# Overview
We solve scattered files across multiple devices by providing a single file system that can be accessed on all their devices.

We solve scattered applications across multiple operating systems and computers by providing applications that work on all their computers thru the web browser.

# Startup
## Not on boarded yet
1. User visits the website. 
2. User reads the goals of the project.
4. User sign ups with userphrase and password.
5. User downloads extensions and desktop applications.
6. User uses applications in web browser and computer as normal.

## On boarded
1. User visits the website.
2. Recent activities show.
3. User clicks activity and begins working.
4. Files and such begin automatically synchronizing

# Application home screen
## Logging in screen.
Notice that it stays simple and minimal. 

![](../../Images/01-Login.png)

## Signup screen.
Notice that the user phrase is generated already. The user does NOT get to choose their user phrase.

![](../../Images/01-Signup.png)

## The web application home screen provides on first launch.

![](../../Images/01-First-Time.png)

## The web application home screen provides on future launches.
Notice that each box contains three things:

1. Name of the activity.
2. Preview of activity (big picture).
3. Provider of activity (small picture).

![](../../Images/01-Returning-Time.png)

# Application Activity

## Sample Activity
Notice that everything except the Home and + are under the Provider's control.

![](../../Images/01-Provider.png)

## Open Activity
Opening activity takes you to the below view.

![](../../Images/01-Create-Open-View.png)

The weird ^| symbol to the left of /path/to/whatever represents the "Up One Directory Button"

After selecting the activity tap open to launch the activity.

## Changing Activity Provider
Changing activity provider requires clicking the provider icon instead of activity.

![](../../Images/01-Change-Activity-Provider.png)

Note that if the user decides to stick with the same provider they can click <--.

Upon selecting a new activity provider, by scrolling and searching. The new activity will launch.

Installing a new activity provider via the + will automatically assign it to the activity and will launch the activity also.

## Adding Activity Provider Search

Searching for a new activity provider.

![](../../Images/01-Activity-Provider-Search.png)

When users select the app image,text does nothing automatically like most other process like creating an activity or changing an activity.

User must click install button, which installs and returns to previous screen (whatever screen that would be).

If the user clicks the > button, opens up full description for the activity provider and they can click back.

## Adding Activity Provider Detailed

Provides more information on a particular activity provider.

![](../../Images/01-Activity-Provider-Detailed.png)

Provides a back button to get back to search results (left of row of images).

Provides gallery of images and full text descritions.


## Creating Activity (Basic)

This occurs after tapping the + button once you are in your chosen directory.

![](../../Images/01-Create-Activity-Basic.png)

The back and path are disabled but kept in view so you know where this activity will land.

The big image in the middle will be the default activity logo. On the left will be activity suggestions based on file name. 

The create button will create a blank activity that is not assigned to any app.

Selecting a small image on the left will create the empty file and launch that activity.

## Creating Activity (Advanced)

If the activity suggestions are not what you need, tapping "Other" will show the Change Activity Provider. Upon selecting a different Activity Provider the activity will launch.


## Closing 
This occurs automatically, the user does not do anything. An activity will automatically suspend if it has not been focused on for the last 2 minutes. 

Whenever a user selects home, the activity they just left will be the first activity they can click on the home screen.