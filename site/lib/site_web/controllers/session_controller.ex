defmodule SiteWeb.SessionController do
    use SiteWeb, :controller

    def new(conn, _) do
        render(conn, "new.html")
    end

    def create(conn, %{"session" => %{"userphrase" => userphrase, "passphrase" => passphrase}}) do
        case Site.Accounts.authenticate_by_userphrase_and_passphrase(userphrase, passphrase) do
            {:ok, user} ->
                conn
                |> SiteWeb.Auth.login(user)
                |> put_flash(:info, "Welcome Back!")
                |> redirect(to: Routes.page_path(conn, :index))

            {:error, _reason} ->
                conn
                |> put_flash(:error, "Invalid userphrase/passphrase combination")
                |> render("new.html")
        end
    end
end