defmodule SiteWeb.UserController do
    use SiteWeb, :controller

    alias Site.Accounts.User
    alias Site.Accounts

    def new(conn, _params) do
        changeset = Accounts.registration_changeset(%User{}, %{})
        render(conn, "new.html", changeset: changeset)
    end

    def create(conn, %{"user" => user_params}) do
        case Accounts.register_user(user_params) do
            {:ok, user} ->
                conn
                |> SiteWeb.Auth.login(user)
                |> put_flash(:info, "Account created!")
                |> redirect(to: Routes.page_path(conn, :index))
            {:error, %Ecto.Changeset{} = changeset} ->
                render(conn, "new.html", changeset: changeset)
        end
    end
end