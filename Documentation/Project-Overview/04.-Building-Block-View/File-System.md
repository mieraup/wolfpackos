# File System
## Overview

Provide fast and just in time information system.

Current idea:
    
Look at ReiserFS,Btrfs,ZFS,

Filesystem should be built to support large scale sharing.

Files are referenced in bursts.That is once referenced will most liekly be refereced again.
Mostly reading, less writing.
Strong Constistency. Read MUST return all writes that proceeded it.
Data integrity
Dataspaces are what users access are kept in sync between servers providing the relay system.
Failure is norm,File content repleication
Synchronization Feature ----

ZFS for scaling.

Goals:
    - Read and Write to Target System (User is responsible for preventing file system clobbering or data inconsistencies)
        In the future we can have conflict resolution occur on Storage System on Users computer
    - Caching till Client Online to deliver.
    - Transparent Synchronization with other Federated Storage Systems 
        This allows user to continue updating, viewing or deleting the file as though it has already been synced regardless of if that is actually the case.
    - Filesystem is composed of 3 parts
        Essential Data
            Contains Filename and Path
        Meta Data
            Links to 1 or more meta data which is a key-value system
        File Data
            Actual File Content