### WebOS Kernel

Provide appropriate API permissions to all systems.

NOTE: Unlike App, GUI and Storage which sit inside Iframe, Kernel is top level.

Provides API access to App,GUI and Storage.

App and GUI are initiated by the App and GUI respectively.
Communication to storage is initiated from Kernel.
All communication is promise based.



App API:
    Starting up app,app has payload of fileData
    - update(newFileData) Pass in full file that contains updated sections.

GUI API:
    - list(path) Return null if nothing exists, otherwise array of strings that are all absolute paths
    - getProviderForPath(path) Return image that denotes the provider or null if there is no provider for that path.
    - setProviderForPath(path,providerID) Provider id is org.providername.en.some.name
    - listProviders(path) Return array of full providerID
    - create(path) Returns true if successfully created empty activity
    - create(path,provider) Returns true if successfully created activity with provider
    - launch(path) Create/Resume activity

Storage API:
    - createFolder(path,mode) Returns true if successfully created in mode, otherwise false
    - createFile(path,mode,data) Returns true if successfully created in mode, otherwise false
    - listFolder(path,mode) Return array of absolutePaths that DO NOT include mode, otherwise return null if path or mode does not exist.
    - deleteFileOrFolder(path,mode) Return true if successfully deleted file or folder, otherwise false.
    - getFile(path,mode) Return file data as Uint8 otherwise null if file does not exist in path for mode.