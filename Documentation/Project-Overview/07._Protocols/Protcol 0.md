Protocol 0 
    - accessed thru the endpoint /api/0/
    - supported till 2020-10-01T05:00:00.000Z CST

    Supported features
        authenticate/new

            Client to server 
            {
                user-phrase: "user phrase",
                password: "password"
            }

            Server to Client
            Either:
                Redirect to homescreen

                Or
            
                Show error on signup page
        authenticate/connect

          Client to server 
            {
                user-phrase: "user phrase",
                password: "password"
            }

            Server to Client
            Either:
                Redirect to homescreen

                Or
            
                Show error on login page

    