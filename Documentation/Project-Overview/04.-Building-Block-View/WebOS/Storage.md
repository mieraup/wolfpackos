### WebOS Storage 

Provide simple API to store and get things.

Made up of two parts:

1) Cached content.
2) Connection to a Storage System to get content that is non cached.

-  Storage provides several API commands.
    - createFolder(path,mode) Returns true if successfully created in mode, otherwise false
    - createFile(path,mode,data) Returns true if successfully created in mode, otherwise false
    - listFolder(path,mode) Return array of absolutePaths that DO NOT include mode, otherwise return null if path or mode does not exist.
    - deleteFileOrFolder(path,mode) Return true if successfully deleted file or folder, otherwise false.
    - getFile(path,mode) Return file data as Uint8 otherwise null if file does not exist in path for mode.