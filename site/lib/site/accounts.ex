defmodule Site.Accounts do
    alias Site.Accounts.User;
    alias Site.Repo

    def change_user(%User{} = user) do
        User.changeset(user, %{})
    end

    def create_user(attrs \\ %{}) do
        %User{}
        |> User.changeset(attrs)
        |> Repo.insert()
    end

    def registration_changeset(%User{} = user, params) do
        User.registration_changeset(user, params)
    end

    def register_user(attrs \\ %{}) do
        %User{}
        |> User.registration_changeset(attrs)
        |> Repo.insert()
    end

    def get_user(id) do
        Repo.get(User, id)
    end

    def get_user!(id) do
        Repo.get!(User, id)     
    end

    def get_user_by(params) do
        Repo.get_by(User,params)
    end

    def list_users do
        Repo.all(User)
    end

    def authenticate_by_userphrase_and_passphrase(userphrase, passphrase) do
        user = get_user_by(userphrase: userphrase)

        cond do
            user && Pbkdf2.verify_pass(passphrase, user.password_hash) ->
                {:ok, user}
            
            user ->
                {:error, :unauthorized}

            true ->
                Pbkdf2.no_user_verify()
                {:error, :not_found}
        end
    end

end