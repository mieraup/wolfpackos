defmodule Site.Repo.Migrations.CreateUsers do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :userphrase, :string
      add :password_hash, :string

      timestamps()
    end

    create unique_index(:users, [:userphrase])
  end
end
