# WolfPack OS
### Because a Pack does better than an Individual

## Goals
    - Everything Everywhere
        (Access items from your phone, computer, server... it just works)
    - Redundancy and Simplicity at every step
## Issues and Milestones

Located at https://develop.projectflame.org

## Mirrors

Located at
- https://gitlab.com/mieraup/wolfpackos
- https://github.com/mieraup/wolfpackos
- https://bitbucket.com/mieraup/wolfpackos
## Version 1.0.0
### Web
    User View
        - Check Email
        - Listen to music
        - Access arbitrary files
    Dev View
        - Kernel 1.0.0 api
        - Architecture fully setup
### Desktop
    User View
        - Simple application that acts as file bridge
    Dev View
        - Everything is represented as Linux style files
### Server
    User View
        -Serving Web OS
    Dev View
        - Nothing yet here

## Licensing 
Unless otherwise noted. All Code and other content is licensed under GNU GENERAL PUBLIC LICENSE Version 3 or later.